package com.realdolmen.spring.blog.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true,
                            prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        String pwQ = "select user_name, password, true from author where lower(user_name) = lower(?)";
        String roleQ = "select user_name, role from author where lower(user_name) = lower(?)";
        auth.jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery(pwQ)
            .authoritiesByUsernameQuery(roleQ);
//        auth.inMemoryAuthentication()
//            .withUser("user")
//            .password("user")
//            .roles("user")
//            .and()
//            .withUser("admin")
//            .password("admin")
//            .roles("admin");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
            .loginPage("/login")
            .and()
            .logout()
            .logoutUrl("/mylogout")
            .logoutSuccessUrl("/")
            .and()
//            .csrf()
//            .and()
            .rememberMe()
            .tokenValiditySeconds(3600)
            .key("myremembermetokenkey")
            .and()
            .authorizeRequests()
            .regexMatchers("/authors/\\d*/remove")
            .hasRole("ADMIN")
            .regexMatchers("/blog[s]", "/authors")
            .hasAnyRole("USER", "ADMIN")
            .regexMatchers("/", "/login", "/js/.*\\.js", "/css/.*\\.css")
            .permitAll()
            .anyRequest()
            .denyAll();
    }
}
